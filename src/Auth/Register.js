import React, {Component} from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text,
  StyleSheet,
  TextInput,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/AntDesign';
import {set} from 'react-native-reanimated';
Icon.loadFont();

const width = Dimensions.get('window').width;
const facebookIcon = <Icon name="facebook-square" color="white" size={15} />;
const googleIcon = <Icon name="google" color="white" size={15} />;

export default class LoginScreen extends Component {
  static navigationOptions = {
    headerShown: true,
  };
  state = {
    isShowPassword: false,
    text: '',
  };
  _email = () => {
    this.props.navigation.navigate('Login');
  };
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', backgroundColor: 'white'}}>
        <Image
          style={{
            width: width,
            height: 200,
            backgroundColor: 'blue',
          }}
          source={require('../img/sayur.png')}
        />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 20,
            marginVertical: 10,
            width: (width * 4) / 5,
          }}>
          <Text style={{textAlign: 'center', fontWeight: '500'}}>
            Login untuk mempermudah anda melacak dan memesan sayur anda
          </Text>
        </View>
        <View style={{...styles.buttonAuth, flexDirection: 'row'}}>
          <Text
            style={{
              flex: 5,
              color: 'white',
              textAlign: 'left',
            }}>
            Email{' '}
          </Text>
          <Text style={{flex: 1, color: 'white'}}>:</Text>
          <TextInput
            style={{flex: 15, color: 'white'}}
            placeholder="Masukkan alamat email"
            placeholderTextColor="grey"
          />
        </View>
        <View style={{...styles.buttonAuth, flexDirection: 'row'}}>
          <Text
            style={{
              flex: 5,
              color: 'white',
              textAlign: 'left',
            }}>
            Password{' '}
          </Text>
          <Text style={{flex: 1, color: 'white'}}>:</Text>
          <TextInput
            style={{flex: 15, color: 'white'}}
            placeholder="Masukkan password"
            placeholderTextColor="grey"
            secureTextEntry={true}
          />
        </View>
        <View style={{...styles.buttonAuth, flexDirection: 'row'}}>
          <Text
            style={{
              flex: 5,
              color: 'white',
              textAlign: 'left',
            }}>
            Ulangi Password
          </Text>
          <Text style={{flex: 1, color: 'white'}}>:</Text>
          <TextInput
            style={{flex: 15, color: 'white'}}
            placeholder="Masukkan password"
            placeholderTextColor="grey"
            secureTextEntry={true}
          />
        </View>
        <TouchableOpacity
          style={styles.buttonAuth}
          onPress={() => this._email()}>
          <Text
            style={{color: 'white', textAlign: 'center', fontWeight: 'bold'}}>
            Daftar sekarang
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonAuth: {
    backgroundColor: 'green',
    padding: 10,
    width: (width * 4) / 5,
    borderRadius: 5,
    marginVertical: 10,
  },
});
