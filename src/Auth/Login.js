import React, {Component} from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text,
  StyleSheet,
    TextInput,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/AntDesign';
import {set} from 'react-native-reanimated';
Icon.loadFont();

const width = Dimensions.get('window').width;
const facebookIcon = <Icon name="facebook-square" color="white" size={15} />;
const googleIcon = <Icon name="google" color="white" size={15} />;

export default class LoginScreen extends Component {
  static navigationOptions = {
    headerShown: true,
  };
  state = {
    isShowPassword: false,
  };
  _register = () => {
    this.props.navigation.navigate('Register');
  };
  _authSuccess = () => {
      this.props.navigation.navigate('Main');
  };
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', backgroundColor: 'white'}}>
        <Image
          style={{
            width: width,
            height: 200,
            backgroundColor: 'blue',
            marginBottom: 30,
          }}
          source={require('../img/sayur.png')}
        />
        <View style={{...styles.buttonAuth, flexDirection: 'row'}}>
          <Text
            style={{
              flex: 5,
              color: 'white',
              textAlign: 'left',
            }}>
            Email{' '}
          </Text>
          <Text style={{flex: 1, color: 'white'}}>:</Text>
            <TextInput
                style={{flex: 15, color: 'white'}}
                placeholder="Masukkan alamat email"
                placeholderTextColor="grey"
            />
        </View>
        <View style={{...styles.buttonAuth, flexDirection: 'row'}}>
          <Text
            style={{
              flex: 5,
              color: 'white',
              textAlign: 'left',
            }}>
            Password{' '}
          </Text>
          <Text style={{flex: 1, color: 'white'}}>:</Text>
            <TextInput
                style={{flex: 15, color: 'white'}}
                placeholder="Masukkan password"
                placeholderTextColor="grey"
                secureTextEntry={true}
            />
        </View>
        {/*<View style={{width: width * 4 / 5, flexDirection: 'row', alignItems: 'flex-start'}}>*/}
        {/*  <CheckBox*/}
        {/*      value={this.state.isShowPassword}*/}
        {/*      tintColor='green'*/}
        {/*  />*/}
        {/*    <CheckBox*/}
        {/*        value={true}*/}
        {/*        disabled={false}*/}
        {/*    />*/}
        {/*  <Text style={{color: 'grey', fontWeight: '400'}}>Tampilkan Password</Text>*/}
        {/*</View>*/}
        <TouchableOpacity
          style={{
            alignItems: 'flex-start',
            width: (width * 4) / 5,
            flexDirection: 'row',
          }}
          onPress={() => {
            this.setState((prev, props) => ({
              isShowPassword: !this.state.isShowPassword,
            }));
          }}>
          <View
            style={{
              width: 20,
              height: 20,
              borderWidth: 1,
              borderRadius: 10,
              borderColor: 'grey',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: 'grey',
                width: 14,
                height: 14,
                borderRadius: 7,
              }}
            />
          </View>
          <Text style={{color: 'grey'}}>
            {' '}
            {this.state.isShowPassword ? 'Tampilkan Password' : 'Sembunyikan Password'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonAuth}
          onPress={() => this._authSuccess()}>
          <Text
            style={{color: 'white', textAlign: 'center', fontWeight: 'bold'}}>
            Masuk sekarang
          </Text>
        </TouchableOpacity>
        <View style={{alignItems: 'center', flexDirection: 'row'}}>
          <Text style={{textAlign: 'center', fontWeight: '200'}}>
            Lupa password?{' '}
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                textDecorationLine: 'underline',
                color: 'green',
                fontWeight: '600',
              }}>
              Ketuk disini
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: (width * 4) / 5,
            flexDirection: 'row',
            marginTop: 40,
          }}>
          <Text
            style={{
              flex: 1,
              fontWeight: '200',
              paddingVertical: 20,
              textAlign: 'left',
            }}>
            Belum punya akun?
          </Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              style={{
                backgroundColor: 'green',
                padding: 10,
                borderRadius: 5,
                marginVertical: 10,
              }}
              onPress={() => this._register()}>
              <Text style={{color: 'white'}}>Daftar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonAuth: {
    backgroundColor: 'green',
    padding: 10,
    width: (width * 4) / 5,
    borderRadius: 5,
    marginVertical: 10,
  },
});
