import React, {Component} from 'react';
import {View, Text, Dimensions, Image} from 'react-native';

const THREE_SECONDS = 3000;

export default class SplashScreen extends Component {
  async componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Auth');
    }, THREE_SECONDS);
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: 'green',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image style={{height: 200, width: 200}} source={require('../img/icon.png')} />
      </View>
    );
  }
}
