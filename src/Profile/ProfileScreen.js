import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image,
  TextInput,
  FlatList,
} from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
EntypoIcon.loadFont();
FontAwesomeIcon.loadFont();

const width = Dimensions.get('window').width;
const editIcon = <EntypoIcon name="edit" size={20} color="grey" />;

const AKUN = [
  {
    id: '1',
    title: 'Pesanan',
    icon: <FontAwesomeIcon name="list-alt" size={20} color="grey" />,
  },
  {
    id: '2',
    title: 'Masukkan kode promo',
    icon: <EntypoIcon name="edit" size={20} color="grey" />,
  },
  {
    id: '3',
    title: 'Ajak teman  pakai kang sayur',
    icon: <FontAwesomeIcon name="group" size={20} color="grey" />,
  },
  {
    id: '4',
    title: 'Pilihan bahasa',
    icon: <EntypoIcon name="language" size={20} color="grey" />,
  },
  {
    id: '5',
    title: 'Bantuan',
    icon: <EntypoIcon name="help" size={20} color="grey" />,
  },
];

const INFO_OTHER = [
  {
    id: '1',
    title: 'Kebijakan Privacy',
    icon: <FontAwesomeIcon name="list-alt" size={20} color="grey" />,
  },
  {
    id: '2',
    title: 'Ketentuan Layanan',
    icon: <EntypoIcon name="edit" size={20} color="grey" />,
  },
  {
    id: '3',
    title: 'Beri Kami Nilai',
    icon: <FontAwesomeIcon name="group" size={20} color="grey" />,
  },
];

function Item({title, icon}) {
  return (
    <TouchableOpacity>
      <View style={{height: 30, flexDirection: 'row', marginVertical: 10}}>
        <View style={{flex: 1}}>{icon}</View>
        <Text style={{fontWeight: 'bold', fontSize: 14, flex: 6}}>{title}</Text>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          <EntypoIcon name="chevron-right" size={20} color="grey" />
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default class ProfileScreen extends Component {
  static navigationOptions = {
    headerShown: false,
    headerVisible: false,
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          flex: 1,
          opacity: 0.5,
          alignItems: 'flex-end',
          backgroundColor: 'grey',
        }}
      />
    );
  };
  render() {
    return (
      <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
        <ScrollView>
          <View style={styles.container}>
            <Text
              style={{
                fontSize: 24,
                fontWeight: 'bold',
                paddingHorizontal: 10,
                marginVertical: 10,
              }}>
              Profil Saya
            </Text>
            <View style={{marginVertical: 10}}>
              <View style={{flexDirection: 'row', height: 40}}>
                <Image
                  style={{
                    alignItems: 'flex-start',
                    height: 40,
                    width: 40,
                    borderRadius: 20,
                  }}
                  source={require('../img/test.png')}
                />
                <View style={{flex: 4, marginLeft: 10}}>
                  <Text style={{fontWeight: 'bold', fontSize: 18}}>Anonim</Text>
                  <Text style={{color: 'grey'}}>+628123456789</Text>
                  <Text style={{color: 'grey'}}>example@example.com</Text>
                </View>
                <TouchableOpacity
                  style={{
                    alignItems: 'flex-end',
                    flex: 1,
                    paddingHorizontal: 10,
                  }}>
                  {editIcon}
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginVertical: 20}}>
              <Text style={{fontWeight: 'bold'}}>Akun</Text>
            </View>
            <FlatList
              data={AKUN}
              renderItem={({item}) => (
                <Item title={item.title} icon={item.icon} />
              )}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              keyExtractor={item => item.id}
            />
            <View style={{marginBottom: 20, marginTop: 0}}>
              <Text style={{fontWeight: 'bold'}}>Info Lainnya</Text>
            </View>
            <FlatList
              data={INFO_OTHER}
              renderItem={({item}) => (
                <Item title={item.title} icon={item.icon} />
              )}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              keyExtractor={item => item.id}
            />

            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 20,
                height: 40,
                width: '100%',
                borderColor: 'red',
                borderWidth: 1,
                marginTop: 20,
              }}>
              <Text style={{color: 'red'}}>Keluar</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 16,
    backgroundColor: 'white',
    flex: 1,
  },
});
