import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image,
  TextInput,
} from 'react-native';
import SearchBar from 'react-native-search-bar';
import CardView from 'react-native-cardview';
import Icon from 'react-native-vector-icons/AntDesign';

const width = Dimensions.get('window').width;
const searchIcon = <Icon name="search" size={15} color="white" />;

export default class HomeScreen extends Component {
  static navigationOptions = {
    headerShown: false,
    headerVisible: false,
  };
  render() {
    return (
      <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
        <ScrollView style={styles.container}>
          <Text
            style={{
              fontSize: 24,
              fontWeight: 'bold',
              paddingHorizontal: 10,
              marginVertical: 10,
            }}>
            Hi Anonim
          </Text>
          <TextInput
            style={{
              backgroundColor: 'green',
              paddingVertical: 10,
              paddingHorizontal: 20,
              borderRadius: 8,
              marginVertical: 10,
              color: 'white',
            }}
            placeholder="Yuk cari produk segarnya..."
            placeholderTextColor="white"
          />
          {/*<SearchBar ref="searchBar" placeholder="Search" style={{height: 30}}/>*/}
          <View style={{flexDirection: 'row', marginVertical: 10}}>
            <CardView
              cardElevation={4}
              cardMaxElevation={4}
              cornerRadius={20}
              style={styles.card}>
              <TouchableOpacity>
                <Image
                  style={{height: 60, width: 60}}
                  source={require('../img/sayur.png')}
                />
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                  Sayur Segar
                </Text>
              </TouchableOpacity>
            </CardView>
            <CardView
              cardElevation={4}
              cardMaxElevation={4}
              cornerRadius={20}
              style={styles.card}>
              <TouchableOpacity>
                <Image
                  style={{height: 60, width: 60}}
                  source={require('../img/sayur.png')}
                />
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                  Buah Segar
                </Text>
              </TouchableOpacity>
            </CardView>
            <CardView
              cardElevation={4}
              cardMaxElevation={4}
              cornerRadius={20}
              style={styles.card}>
              <TouchableOpacity>
                <Image
                  style={{height: 60, width: 60}}
                  source={require('../img/sayur.png')}
                />
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                  Karbohidrat
                </Text>
              </TouchableOpacity>
            </CardView>
          </View>
          <View style={{marginVertical: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  alignItems: 'flex-start',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                Kang Sayur
              </Text>
              <TouchableOpacity
                style={{
                  alignItems: 'flex-end',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                <Text>Lihat Semua</Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={{flexDirection: 'row'}} horizontal={true}>
              <CardView
                cardElevation={4}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card1}>
                <TouchableOpacity>
                  <Image
                    style={{height: 120, width: 120}}
                    source={require('../img/sayur.png')}
                  />
                  <Text style={{fontSize: 14, fontWeight: '500'}}>
                    Kang Mamat
                  </Text>
                </TouchableOpacity>
              </CardView>
              <CardView
                cardElevation={4}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card1}>
                <TouchableOpacity>
                  <Image
                    style={{height: 120, width: 120}}
                    source={require('../img/sayur.png')}
                  />
                  <Text style={{fontSize: 14, fontWeight: '500'}}>
                    Kang Ucup
                  </Text>
                </TouchableOpacity>
              </CardView>
            </ScrollView>
          </View>
          <View style={{marginVertical: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  alignItems: 'flex-start',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                Kang Promo
              </Text>
              <TouchableOpacity
                style={{
                  alignItems: 'flex-end',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                <Text>Lihat Semua</Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={{flexDirection: 'row'}} horizontal={true}>
              <CardView
                cardElevation={2}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card2}>
                <TouchableOpacity>
                  <Image
                    style={{
                      margin: 0,
                      borderRadius: 20,
                      height: 180,
                      width: width - 60,
                    }}
                    source={require('../img/sayur.png')}
                  />
                </TouchableOpacity>
              </CardView>
              <CardView
                cardElevation={2}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card2}>
                <TouchableOpacity>
                  <Image
                    style={{
                      margin: 0,
                      borderRadius: 20,
                      height: 180,
                      width: width - 60,
                    }}
                    source={require('../img/sayur.png')}
                  />
                </TouchableOpacity>
              </CardView>
            </ScrollView>
          </View>
          <View style={{marginVertical: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  alignItems: 'flex-start',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                Kang Sehat
              </Text>
              <TouchableOpacity
                style={{
                  alignItems: 'flex-end',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                <Text>Lihat Semua</Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={{flexDirection: 'row'}} horizontal={true}>
              <CardView
                cardElevation={2}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card2}>
                <TouchableOpacity>
                  <Image
                    style={{
                      margin: 0,
                      borderRadius: 20,
                      height: 180,
                      width: width - 60,
                    }}
                    source={require('../img/sayur.png')}
                  />
                </TouchableOpacity>
              </CardView>
              <CardView
                cardElevation={2}
                cardMaxElevation={4}
                cornerRadius={20}
                style={styles.card2}>
                <TouchableOpacity>
                  <Image
                    style={{
                      margin: 0,
                      borderRadius: 20,
                      height: 180,
                      width: width - 60,
                    }}
                    source={require('../img/sayur.png')}
                  />
                </TouchableOpacity>
              </CardView>
            </ScrollView>
          </View>
          <View style={{marginVertical: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  alignItems: 'flex-start',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                Kang Favorite
              </Text>
              <TouchableOpacity
                style={{
                  alignItems: 'flex-end',
                  flex: 1,
                  paddingHorizontal: 10,
                }}>
                <Text>Lihat Semua</Text>
              </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={{flexDirection: 'row', flexWrap: 'wrap', flex: 1}}>
                <CardView
                  cardElevation={2}
                  cardMaxElevation={4}
                  cornerRadius={20}
                  style={styles.card3}>
                  <TouchableOpacity>
                    <Image
                      style={{height: 120, width: 120}}
                      source={require('../img/sayur.png')}
                    />
                    <Text style={{fontSize: 14, fontWeight: '500'}}>
                      Kang Mamat
                    </Text>
                  </TouchableOpacity>
                </CardView>
                <CardView
                  cardElevation={2}
                  cardMaxElevation={4}
                  cornerRadius={20}
                  style={styles.card3}>
                  <TouchableOpacity>
                    <Image
                      style={{height: 120, width: 120}}
                      source={require('../img/sayur.png')}
                    />
                    <Text style={{fontSize: 14, fontWeight: '500'}}>
                      Kang Ucup
                    </Text>
                  </TouchableOpacity>
                </CardView>
              </View>
            </ScrollView>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 16,
    backgroundColor: 'white',
    flex: 1,
  },
  card: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    height: 120,
  },
  card1: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    height: 180,
    width: 180,
  },
  card2: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    height: 180,
    width: width - 60,
  },
  card3: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    height: 240,
    width: width / 2 - 20,
  },
});
